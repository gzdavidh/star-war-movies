const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.css$/,
        loader: ['style-loader', 'css-loader']
      },
    ]
  },
  resolve: {
    alias : {
      '@' : path.resolve(__dirname, './src'),
    },
    extensions : ['.js', '.jsx'],
    modules : ['src', 'node_modules']
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html',
    })
  ],
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8000',
      }
    }
  }
};