import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ReactStars from 'react-stars';
import './Panel.css';

const Panel = (props) => {
  const {
    children,
    className,
    heading,
    posterSrc,
    onChange,
    value
  } = props;

  return (
    <div className={classNames('panel', className)}>
      { posterSrc && <img className='poster' src={posterSrc} alt='Movie poster' />}
      <ReactStars
        className='favorite'
        count={1}
        onChange={onChange}
        size={30}
        color2='#ffd700'
        half={false}
        value={value}
      />
      { heading && <h2>{ heading }</h2> }
      { children }
    </div>
  );
};

Panel.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  className: PropTypes.string,
  heading: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  posterSrc: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.number,
};

Panel.defaultProps = {
  heading: undefined,
  className: undefined,
  value: 0
};

export default Panel;
