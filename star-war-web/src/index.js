import RootComponent from '@/components/RootComponent';
import React from 'react';
import ReactDOM from 'react-dom';
import store from './store';
import './styles/styles.css'

const routes = require('./routes/index').default(store);
const MOUNT_NODE = document.getElementById('root');

ReactDOM.render(
  <RootComponent store={store} routes={routes} />,
  MOUNT_NODE
);
