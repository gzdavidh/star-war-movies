import createStore from './createStore';

const store = createStore();

export const { getState } = store;
export default store;
