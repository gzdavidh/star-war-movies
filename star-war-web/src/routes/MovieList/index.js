import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import addFavorite from '@/reducers/app/actions/addFavorite';
import removeFavorite from '@/reducers/app/actions/removeFavorite';
import fetchFavorites from '@/reducers/app/actions/fetchFavorites';
import MovieList from './MovieList';

const mapStateToProps = state => ({
  favoriteMovies: state.app.favoriteMovies
});
const mapDispatchToProps = {
  addFavorite,
  removeFavorite,
  fetchFavorites,
};

const MovieListRedux = connect(mapStateToProps, mapDispatchToProps)(MovieList);
const MovieListRouter = withRouter(MovieListRedux);

export default MovieListRouter;
