import starWarMovies from '@/data/star-war-movies.json';
import { Panel } from '@/components';
import PropTypes from 'prop-types';
import Switch from 'react-switch';
import { Container, Row, Col } from 'reactstrap';
import React from 'react';
import './MovieList.css';

class MovieList extends React.Component {
  static propTypes = {
    favoriteMovies: PropTypes.arrayOf(PropTypes.string),
    addFavorite: PropTypes.func.isRequired,
    removeFavorite: PropTypes.func.isRequired,
    fetchFavorites: PropTypes.func.isRequired,
  }

  static defaultProps = {
    favoriteMovies: []
  }

  constructor(props) {
    super(props);
    this.state = {
      isMacheteOrder: false,
      movieList: [],
    };
    this.toggleMacheteOrder = this.toggleMacheteOrder.bind(this);
    this.sortMovieBy = this.sortMovieBy.bind(this);
    this.updateFavorite = this.updateFavorite.bind(this);
  }

  componentDidMount() {
    this.sortMovieBy('episode');
    this.props.fetchFavorites();
  }

  sortMovieBy(criteria) {
    const { movies } = starWarMovies;

    const sortedList = movies.filter(movie => (movie.position[criteria] !== ''))
      .sort((a, b) => (a.position[criteria] - b.position[criteria]));

    this.setState({
      movieList: sortedList
    });
  }

  toggleMacheteOrder() {
    this.sortMovieBy(this.state.isMacheteOrder ? 'episode' : 'machete');
    this.setState(prevState => ({
      isMacheteOrder: !prevState.isMacheteOrder
    }));
  }

  updateFavorite(imdbID) {
    const { favoriteMovies, removeFavorite, addFavorite } = this.props;

    if (favoriteMovies.includes(imdbID)) {
      removeFavorite(imdbID);
    } else {
      addFavorite(imdbID);
    }
  }

  render() {
    const { favoriteMovies } = this.props;
    const { movieList } = this.state;

    return (
      <Container>
        <Row>
          <Col md={9}>
            <Switch
              checked={this.state.isMacheteOrder}
              onChange={this.toggleMacheteOrder}
              onColor='#86d3ff'
              onHandleColor='#2693e6'
              handleDiameter={20}
              uncheckedIcon={false}
              checkedIcon={false}
              boxShadow='0px 1px 5px rgba(0, 0, 0, 0.6)'
              activeBoxShadow='0px 0px 1px 10px rgba(0, 0, 0, 0.2)'
              height={20}
              width={48}
              id='machete-switch'
            />
            <label htmlFor='machete-switch'>
              <span className='toggle'>Switch to machete order</span>
            </label>
          </Col>
        </Row>
        <Row>
          <Col>
            {
              movieList.map((movie) => {
                const isFavorite = favoriteMovies.length > 0 && favoriteMovies.includes(movie.imdbID);

                return (
                  <Panel
                    heading={`${movie.Title} (${movie.Year})`}
                    posterSrc={movie.Poster}
                    key={movie.imdbID}
                    value={isFavorite ? 1 : 0}
                    onChange={() => { this.updateFavorite(movie.imdbID); }}
                  >
                    <div className='subheading'>
                      {`${movie.Rated} | ${movie.Runtime} | ${movie.Genre} | ${movie.Released}`}
                    </div>
                    <div className='credit'>Director: {movie.Director}</div>
                    <div className='credit'>Writer: {movie.Writer}</div>
                    <div className='credit'>Stars: {movie.Actors}</div>
                    <div className='plot'>{movie.Plot}</div>
                  </Panel>
                );
              })
            }
          </Col>
        </Row>
      </Container>
    );
  }
}

export default MovieList;
