import React from 'react';
import { Route, Switch } from 'react-router-dom';
import MovieList from './MovieList';

export const appRoutes = () => (
  <Switch>
    <Route path='/'>
      <MovieList />
    </Route>
  </Switch>
);


export default appRoutes;
