import {
  ADD_FAVORITE,
  ADD_FAVORITE_SUCCESS,
  ADD_FAVORITE_FAILURE,
  REMOVE_FAVORITE,
  REMOVE_FAVORITE_SUCCESS,
  REMOVE_FAVORITE_FAILURE,
  FETCH_FAVORITE,
  FETCH_FAVORITE_SUCCESS,
  FETCH_FAVORITE_FAILURE
} from './constants/favoriteUpdateStatus';

export const initialState = {
  starWarMovies: [],
  favoriteMovies: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FAVORITE_SUCCESS:
      return { ...state, favoriteMovies: action.favoriteMovies };
    case ADD_FAVORITE_SUCCESS:
      return { ...state, favoriteMovies: [...state.favoriteMovies, action.add] };
    case REMOVE_FAVORITE_SUCCESS:
      return { ...state, favoriteMovies: state.favoriteMovies.filter(_ => (_ !== action.remove)) };

    case ADD_FAVORITE_FAILURE:
    case REMOVE_FAVORITE_FAILURE:
      return { ...state, favoriteMovies: [...state.favoriteMovies] };

    case ADD_FAVORITE:
    case REMOVE_FAVORITE:
    case FETCH_FAVORITE:
    case FETCH_FAVORITE_FAILURE:
    default:
      return state;
  }
};
