import {
  ADD_FAVORITE,
  ADD_FAVORITE_SUCCESS,
  ADD_FAVORITE_FAILURE
} from '../constants/favoriteUpdateStatus';

export default imdbId => async function addFavorite(dispatch) {
  dispatch({ type: ADD_FAVORITE });

  const onSuccess = () => {
    dispatch({ type: ADD_FAVORITE_SUCCESS, add: imdbId });

    return { success: true };
  };
  const onFailure = () => {
    dispatch({ type: ADD_FAVORITE_FAILURE });

    return { success: false };
  };

  try {
    const response = await (await fetch(`/api/favorites/${imdbId}`, {
      method: 'PUT',
    })).json();

    if (response.success) {
      return onSuccess();
    }

    return onFailure();
  } catch (e) {
    console.log(e); // eslint-disable-line

    return onFailure();
  }
};
