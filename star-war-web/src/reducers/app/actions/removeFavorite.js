import {
  REMOVE_FAVORITE,
  REMOVE_FAVORITE_SUCCESS,
  REMOVE_FAVORITE_FAILURE
} from '../constants/favoriteUpdateStatus';

export default imdbId => async function removeFavorite(dispatch) {
  dispatch({ type: REMOVE_FAVORITE });

  const onSuccess = () => {
    dispatch({ type: REMOVE_FAVORITE_SUCCESS, remove: imdbId });

    return { success: true };
  };
  const onFailure = () => {
    dispatch({ type: REMOVE_FAVORITE_FAILURE });

    return { success: false };
  };

  try {
    const response = await (await fetch(`/api/favorites/${imdbId}`, {
      method: 'DELETE',
    })).json();

    if (response.success) {
      return onSuccess();
    }

    return onFailure();
  } catch (e) {
    console.log(e); // eslint-disable-line

    return onFailure();
  }
};
