import {
  FETCH_FAVORITE,
  FETCH_FAVORITE_SUCCESS,
  FETCH_FAVORITE_FAILURE
} from '../constants/favoriteUpdateStatus';

export default () => async function fetchFavorites(dispatch) {
  dispatch({ type: FETCH_FAVORITE });

  const onSuccess = ({ favoriteMovies }) => {
    dispatch({ type: FETCH_FAVORITE_SUCCESS, favoriteMovies });

    return { success: true };
  };
  const onFailure = () => {
    dispatch({ type: FETCH_FAVORITE_FAILURE });

    return { success: false };
  };

  try {
    const response = await (await fetch('/api/favorites', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })).json();

    if (response.success) {
      return onSuccess({ favoriteMovies: response.result.favoriteMovies });
    }

    return onFailure();
  } catch (e) {
    console.log(e); // eslint-disable-line

    return onFailure();
  }
};
