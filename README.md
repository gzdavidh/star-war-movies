# README #

This is a SPA showing the Star War movies.

### How do I get set up? ###

1. use DB.sql to create database tables in mySQL
2. Update DB user name and password in star-war-service/src/main/resources/application.properties
3. In star-war-web folder run command "npm run start:dev" to start up UI server.
4. In star-war-service folder run command "mvn spring-boot:run" to start up the service server.
5. http://localhost:8080/
