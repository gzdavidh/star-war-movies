create datbase demo;

use demo;

create table Favorite_Movie (
    imdb_id varchar(20) not null,
    update_time datetime default CURRENT_TIMESTAMP,
    constraint primary key (imdb_id)
);

create table Request_History (
    history_id bigint AUTO_INCREMENT,
    request_ip varchar(20) not null,
    end_point varchar(100) not null,
    query_string varchar(1024),
    request_time datetime default CURRENT_TIMESTAMP,
    constraint primary key (history_id)
);