package david.demo.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import david.demo.dto.RequestResult;
import david.demo.service.MovieService;

@RestController
public class MovieController {
	private static final Logger logger = LogManager.getLogger(MovieController.class);

	@Autowired
	private MovieService movieService;

	// Read favorite movie list
	@GetMapping("/api/favorites")
	public ResponseEntity<RequestResult> getFavoriteMovies(HttpServletRequest request) {
		RequestResult rr = new RequestResult();
		rr.setResult(movieService.getFavoriteMovies());

		return ResponseEntity.ok(rr);
	}

	// Add to favorite movie list
	@PutMapping("/api/favorites/{imdbId}")
	public ResponseEntity<RequestResult> addFavorite(@PathVariable String imdbId) {
		logger.info("Adding new favorite: " + imdbId);
		if (movieService.addMovieToFavorite(imdbId)) {
			return ResponseEntity.ok(new RequestResult());
		}

		return getGenericErrorResponse();
	}

	// Remove favorite movie list
	@DeleteMapping("/api/favorites/{imdbId}")
	public ResponseEntity<RequestResult> removeFavorite(@PathVariable String imdbId) {
		logger.info("Removing favorite: " + imdbId);
		if (movieService.removeMovieFromFavorite(imdbId)) {
			return ResponseEntity.ok(new RequestResult());
		}

		return getGenericErrorResponse();
	}

	private ResponseEntity<RequestResult> getGenericErrorResponse() {
		RequestResult rr = new RequestResult();
		rr.setSuccess(false);
		return ResponseEntity.badRequest().body(rr);
	}
}
