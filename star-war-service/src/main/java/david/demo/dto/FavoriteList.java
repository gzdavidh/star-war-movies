package david.demo.dto;

import java.util.List;

public class FavoriteList {
	List<String> favoriteMovies;

	public List<String> getFavoriteMovies() {
		return favoriteMovies;
	}

	public void setFavoriteMovies(List<String> favoriteMovies) {
		this.favoriteMovies = favoriteMovies;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{favoriteMovies: [");
		if (favoriteMovies != null) {
			sb.append(String.join(",", favoriteMovies));
		}
		sb.append("]}");
		return sb.toString();
	}
}
