package david.demo.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import david.demo.model.FavoriteMovie;

@Repository
public interface MovieRepository extends JpaRepository<FavoriteMovie, String> {

}
