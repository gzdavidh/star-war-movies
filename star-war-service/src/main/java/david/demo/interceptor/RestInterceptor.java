package david.demo.interceptor;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import david.demo.model.RequestHistory;
import david.demo.service.RequestHistoryService;

@Component
public class RestInterceptor implements HandlerInterceptor {
	private static final Logger logger = LogManager.getLogger(RestInterceptor.class);

	@Autowired
	private RequestHistoryService requestHistoryService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		RequestHistory history = new RequestHistory();
		history.setEndPoint("[" + request.getMethod() + "]" + request.getRequestURI());
		history.setQueryString(getParameters(request));

		String ip = request.getHeader("X-FORWARDED-FOR");
		String ipAddr = (ip == null) ? request.getRemoteAddr() : ip;
		history.setRequestIp(ipAddr);

		requestHistoryService.logRequestHistory(history);

		logger.info("Accessing " + history.getEndPoint());

		return true;
	}

	private String getParameters(HttpServletRequest request) {
		StringBuffer posted = new StringBuffer();
		Enumeration<?> e = request.getParameterNames();
		if (e != null) {
			while (e.hasMoreElements()) {
				if (posted.length() > 1) {
					posted.append("&");
				}
				String curr = (String) e.nextElement();
				posted.append(curr).append("=").append(request.getParameter(curr));
			}
		}

		return posted.toString();
	}
}
