package david.demo.service;

import david.demo.model.RequestHistory;

public interface RequestHistoryService {
	public void logRequestHistory(RequestHistory history) throws Exception;
}
