package david.demo.service;

import david.demo.dto.FavoriteList;

public interface MovieService {
	public boolean addMovieToFavorite(String imdbId);
	public boolean removeMovieFromFavorite(String imdbId);
	public FavoriteList getFavoriteMovies();
}
