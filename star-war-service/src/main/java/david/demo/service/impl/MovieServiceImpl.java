package david.demo.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import david.demo.data.MovieRepository;
import david.demo.dto.FavoriteList;
import david.demo.model.FavoriteMovie;
import david.demo.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService{
	
	private static final Logger logger = LogManager.getLogger(MovieServiceImpl.class);
	
	@Autowired
	private MovieRepository movieRepository;

	@Override
	public boolean addMovieToFavorite(String imdbId) {
		FavoriteMovie favoriteMovie = new FavoriteMovie();
		favoriteMovie.setImdbId(imdbId);
		favoriteMovie.setUpdateTime(LocalDateTime.now());
		try {
			movieRepository.save(favoriteMovie);
			
			return true;
		} catch (Exception e) {
			logger.info("Error adding movie to favorite: " + e.getMessage());
			
			return false;
		}
	}

	@Override
	public boolean removeMovieFromFavorite(String imdbId) {
		try {
			movieRepository.deleteById(imdbId);
			
			return true;
		} catch (Exception e) {
			logger.info("Error removing movie from favorite " + e.getMessage());
			
			return false;
		}	
	}

	@Override
	public FavoriteList getFavoriteMovies() {
		FavoriteList result = new FavoriteList();
		
		try {
			List<FavoriteMovie> movies = movieRepository.findAll();
			List<String> idList = new ArrayList<String>();
			for (Iterator<FavoriteMovie> iterator = movies.iterator(); iterator.hasNext();) {
				FavoriteMovie favoriteMovie = iterator.next();
				idList.add(favoriteMovie.getImdbId());
			}
			result.setFavoriteMovies(idList);
		} catch (Exception e) {
			logger.info("Error retrieving favorite movies " + e.getMessage());
		}
		
		return result;
	}

}
