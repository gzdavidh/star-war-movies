package david.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import david.demo.data.RequestHistoryRepository;
import david.demo.model.RequestHistory;
import david.demo.service.RequestHistoryService;

@Service
public class RequestHistoryServiceImpl implements RequestHistoryService{

	@Autowired
	private RequestHistoryRepository historyRepository;
	
	@Override
	public void logRequestHistory(RequestHistory history) throws Exception {
		historyRepository.saveAndFlush(history);
	}

}
