package david.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarWarMovieApp {

  public static void main(String[] args) {
    SpringApplication.run(StarWarMovieApp.class, args);
  }

}
